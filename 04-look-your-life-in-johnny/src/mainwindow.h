#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class JohnnyPaletteConverter;
class PictureWorker;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    void setJohnnyPaletteConverter(JohnnyPaletteConverter* palette);
    ~MainWindow();

private slots:
    void updatePicture();

private:
    Ui::MainWindow *ui;
    JohnnyPaletteConverter* m_johnnyPalette;
};

#endif // MAINWINDOW_H
