#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileSystemModel>
#include <QSortFilterProxyModel>

#include "johnnypaletteconverter.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFileSystemModel *model = new QFileSystemModel;

    // Sort file and directories
    QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(this);
    model->setRootPath(QDir::currentPath());
    proxyModel -> setSourceModel(model);
    this -> ui -> treeView -> setModel(proxyModel);
    this -> ui -> treeView -> sortByColumn(0, Qt::AscendingOrder);

    // Hide unused column
    for(int i = 1; i < 4; i++)
    {
        this -> ui -> treeView -> setColumnHidden(i, true);
    }

    // Connect on the signal of selection to update
    this -> connect(this -> ui -> treeView, SIGNAL(clicked(QModelIndex)), this, SLOT(updatePicture()));
    this -> connect(this -> ui -> actionQuit, SIGNAL(triggered()), this, SLOT(close()));
}

void MainWindow::setJohnnyPaletteConverter(JohnnyPaletteConverter* palette)
{
    this -> m_johnnyPalette = palette;
}

MainWindow::~MainWindow()
{
    delete ui;
}

 void MainWindow::updatePicture()
 {
     if (this -> m_johnnyPalette == NULL)
         return;

     QModelIndex model_index = this -> ui -> treeView -> currentIndex();
     QString path = model_index.data(QFileSystemModel::FilePathRole).toString();
     QRegExp regexp("^.*\\.(png|jpeg|bmp|jpg)$", Qt::CaseInsensitive);
     bool is_valid = regexp.exactMatch(path);
     if (is_valid == true)
     {
         QImage image(path);
         QPixmap original_pixmap = QPixmap::fromImage(image);
         this -> ui -> image_original -> setPixmap(original_pixmap);
         QImage mosaic_image = this -> m_johnnyPalette -> convertImage(image);
         QPixmap pixmap = QPixmap::fromImage(mosaic_image);
         this -> ui -> image -> setPixmap(pixmap);
     }
     // Si c'est une image, la transformer et l'afficher
 }
