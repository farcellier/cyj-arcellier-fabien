#include <QtGui/QApplication>
#include "johnnypalette.h"
#include "johnnypaletteconverter.h"
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Charger la ressource de la palette
    JohnnyPaletteHandler* handler = new JohnnyPaletteHandler(":/palette/palette.jpal");
    JohnnyPaletteModel* model = new JohnnyPaletteModel(handler);
    handler->setModel(model);
    model->load();

    // Instancier le modèle et la palette
    JohnnyPaletteConverter* converter = new JohnnyPaletteConverter(model);

    MainWindow w;
    w.setJohnnyPaletteConverter(converter);
    w.show();
    
    return a.exec();
}
