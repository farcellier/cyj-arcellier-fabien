#-------------------------------------------------
#
# Project created by QtCreator 2013-03-22T14:13:33
#
#-------------------------------------------------

QT       += testlib core gui

TARGET = tst_mainwindow
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_mainwindow.cpp \
    tst_main.cpp

HEADERS += \
    tst_mainwindow.h

include(../src/src.pri)
