#ifndef TST_MAINWINDOW_H
#define TST_MAINWINDOW_H

#include <QObject>

class tst_MainWindow : public QObject
{
    Q_OBJECT

public:
    explicit tst_MainWindow(QObject* parent = NULL);

private Q_SLOTS:
    void testBasicCase();
};


#endif // TST_MAINWINDOW_H
