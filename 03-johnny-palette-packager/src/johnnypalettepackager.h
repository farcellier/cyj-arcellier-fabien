#ifndef JOHNNYPALETTEPACKAGER_H
#define JOHNNYPALETTEPACKAGER_H

#include <QStringList>

class JohnnyPalettePackager
{
public:
    JohnnyPalettePackager();
    QStringList picturesFromDirectory(QString path);
    void storePaletteFile(QString palette_path, QStringList pictures_path);
};

#endif // JOHNNYPALETTEPACKAGER_H
