#include "johnnypalettepackager.h"

#include <QDir>

#include "johnnypalette.h"
#include "pictureworker.h"

const int PICTURE_SIZE = 20;

JohnnyPalettePackager::JohnnyPalettePackager()
{
}

/**
 * Gets list of path from pictures stored in directory given as parameter
 *
 * TODO Refactoriser pour fabriquer une nouvelle QStringList, non fiable
 */
QStringList JohnnyPalettePackager::picturesFromDirectory(QString path)
{
    QDir directory(path);
    QStringList list_files_path = directory.entryList(QDir::Files);
    QStringList list_pictures_path;

    // Filter the contents of list pictures
    QRegExp valider_picture_path("^.*\\.(png|jpeg|bmp|jpg)$", Qt::CaseInsensitive);
    for(int i = 0; i < list_files_path.count(); i++)
    {
        QString picture_path = list_files_path.at(i);
        if(valider_picture_path.exactMatch(picture_path) == true)
        {
            QString picture_absolute_path = QString("%0/%1").arg(directory.absolutePath(), picture_path);
            list_pictures_path.append(picture_absolute_path);
        }
    }

    return list_pictures_path;
}

/**
 * Using path list of pictures, create a palette package file
 */
void JohnnyPalettePackager::storePaletteFile(QString palette_path, QStringList pictures_path)
{
    JohnnyPaletteHandler handler(palette_path);
    JohnnyPaletteModel model(&handler);
    handler.setModel(&model);

    foreach(QString picture_path, pictures_path)
    {
        QImage* image = new QImage(picture_path);
        if (image != NULL)
        {
            PictureWorker worker(image);
            QImage tmp_image = worker.scaleAndCrop(PICTURE_SIZE);
            worker.setImage(&tmp_image);

            JohnnyPaletteItem item(tmp_image, worker.averageColor());
            model.append(item);
        }
    }

    model.save();
}
