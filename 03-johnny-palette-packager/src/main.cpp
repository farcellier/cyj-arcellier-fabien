#include <iostream>
#include <QtGui/QApplication>
#include <QDir>

#include "johnnypalettepackager.h"

using namespace std;

/**
  * Display help for user in console mode
  */
void displayHelp()
{
    cout << "johnny-palette-packager directory package" << endl;
    cout << QObject::tr("Package the content of a directory in a binary file used in johnny-photo-viewer").constData() << endl;
    cout << "" << endl;
    cout << "  directory directory that contains pictures (jpeg, png, bmp or jpg)" << endl;
    cout << "  package path of package to generate" << endl;
}

int main(int argc, char *argv[])
{
    // If there is less than 3 arguments, display help
    if (argc < 3)
    {
        displayHelp();
        exit(0);
    }

    QDir directory(argv[1]);
    if (directory.exists() == false)
    {
        cerr << "The directory given as argument does not exists " << argv[1];
        exit(1);
    }

    JohnnyPalettePackager packager;
    QStringList pictures_path = packager.picturesFromDirectory(argv[1]);
    packager.storePaletteFile(argv[2], pictures_path);
}
