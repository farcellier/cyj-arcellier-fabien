#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class ThumbValue;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void updatePictures();
    
private:
    Ui::MainWindow *ui;
    QList<ThumbValue*> m_thumbvalueWidgets;
};

#endif // MAINWINDOW_H
