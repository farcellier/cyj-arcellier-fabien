#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QAbstractItemModel>
#include <QDebug>
#include <QFileSystemModel>
#include <QItemSelectionModel>
#include <QModelIndexList>
#include <QSortFilterProxyModel>
#include <QVBoxLayout>

#include "Widget/thumbvalue.h"

/*!
 * Instanciate Main window
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QFileSystemModel *model = new QFileSystemModel;

    // Sort file and directories
    QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(this);
    model->setRootPath(QDir::currentPath());
    proxyModel -> setSourceModel(model);
    this -> ui -> treeView -> setModel(proxyModel);
    this -> ui -> treeView -> sortByColumn(0, Qt::AscendingOrder);

    // Hide unused column
    for(int i = 1; i < 4; i++)
    {
        this -> ui -> treeView -> setColumnHidden(i, true);
    }

    // Connect on the signal of selection to update
    this -> connect(this -> ui -> treeView, SIGNAL(clicked(QModelIndex)), this, SLOT(updatePictures()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * Create thumbnails according to the selection
 */
void MainWindow::updatePictures()
{
    QItemSelectionModel* list = this -> ui -> treeView -> selectionModel();
    QModelIndexList model_lists = list -> selectedIndexes();

    QList<QString> valid_paths;
    for(int i = 0; i < model_lists.count(); i++)
    {
        QModelIndex model_index = model_lists.at(i);
        QString path = model_index.data(QFileSystemModel::FilePathRole).toString();
        QRegExp regexp("^.*\\.(png|jpeg|bmp|jpg)$", Qt::CaseInsensitive);
        bool is_valid = regexp.exactMatch(path);
        if (is_valid == true)
        {
            valid_paths.append(path);
        }
    }

    for(int i = 0; i < valid_paths.count(); i++)
    {
        if (i >= this -> m_thumbvalueWidgets.count())
        {
            ThumbValue* thumb_value = new ThumbValue(valid_paths.at(i));
            this -> m_thumbvalueWidgets.append(thumb_value);
            this -> ui -> thumbnailContents -> addWidget(thumb_value);
        }
        else
        {
            this -> m_thumbvalueWidgets.at(i) -> setPath(valid_paths.at(i));
        }
    }

    // Remove the control that are unused
    for(int i = valid_paths.count(); valid_paths.count() < this -> m_thumbvalueWidgets.count(); i++)
    {
        ThumbValue* thumb_value = this -> m_thumbvalueWidgets.at(valid_paths.count());
        this -> ui -> thumbnailContents -> removeWidget(thumb_value);
        this -> m_thumbvalueWidgets.removeAt(valid_paths.count());
        delete thumb_value;
    }
}
