#include "thumbvalue.h"
#include "ui_thumbvalue.h"

#include <QGraphicsScene>
#include <QPalette>
#include "pictureworker.h"

ThumbValue::ThumbValue(QString path, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ThumbValue),
    m_path(path)
{
    ui->setupUi(this);
    this -> initWidget();
}

ThumbValue::~ThumbValue()
{
    delete ui;
}

void ThumbValue::setPath(QString path)
{
    this -> m_path = path;
    this -> initWidget();
}

/*!
 * Init the contents of the thumbnails
 */
void ThumbValue::initWidget()
{
    const QImage* original = new QImage(this -> m_path);
    if (original != NULL)
    {
        PictureWorker worker(original);

        this -> m_thumbnail = worker.scaleAndCrop(120);
        worker.setImage(&this -> m_thumbnail);
        this -> ui -> image -> setPixmap(QPixmap::fromImage(this -> m_thumbnail));

        QPalette palette;
        QColor color = worker.averageColor();
        palette.setColor(QPalette::Background, color);
        this -> ui -> value -> setAutoFillBackground(true);
        this -> ui -> value -> setPalette(palette);
    }
}
