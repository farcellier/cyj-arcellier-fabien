#ifndef THUMBVALUE_H
#define THUMBVALUE_H

#include <QWidget>


namespace Ui {
    class ThumbValue;
}

/*!
 * Component to display a picture with average value
 */
class ThumbValue : public QWidget
{
    Q_OBJECT

public:
    explicit ThumbValue(QString path, QWidget *parent = 0);
    ~ThumbValue();
    void setPath(QString path);

private:
    void initWidget();
    Ui::ThumbValue *ui;
    QString m_path;
    QImage  m_thumbnail;
};

#endif // THUMBVALUE_H
