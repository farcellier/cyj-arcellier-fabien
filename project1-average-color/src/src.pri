SOURCES  *= $$PWD/mainwindow.cpp \
    $$PWD/Widget/thumbvalue.cpp

HEADERS  *= $$PWD/mainwindow.h \
    $$PWD/Widget/thumbvalue.h

FORMS    *= $$PWD/mainwindow.ui \
    $$PWD/Widget/thumbvalue.ui

INCLUDEPATH *= $$PWD

include("../../02-johnny-library/src/johnny-library-src.pri")
