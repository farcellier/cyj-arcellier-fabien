#ifndef IHANDLER_H
#define IHANDLER_H

class IHandler
{
public:
    virtual void save() = 0;
    virtual void load() = 0;
};

#endif // IHANDLER_H
