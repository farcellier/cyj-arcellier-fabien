#ifndef JOHNNYPALETTECONVERTER_H
#define JOHNNYPALETTECONVERTER_H

#include <QImage>
#include <QColor>
#include <qmath.h>

class JohnnyPaletteItem;
class JohnnyPaletteModel;

class ColorReference
{
public:
    ColorReference(QColor colorReference, JohnnyPaletteItem* item) :
        m_item(item)
    {
        this -> m_blue = colorReference.blue();
        this -> m_red = colorReference.red();
        this -> m_green = colorReference.green();
    }

    int distance(QColor pixel_color) {
        return qPow(pixel_color.blue() - this -> m_blue, 2) +
                qPow(pixel_color.red() - this -> m_red, 2) +
                qPow(pixel_color.green() - this -> m_green, 2);
    }

    JohnnyPaletteItem* item()
    {
        return this -> m_item;
    }

private:
    char m_blue;
    char m_green;
    char m_red;
    JohnnyPaletteItem* m_item;
} ;

class JohnnyPaletteConverter
{
public:
    JohnnyPaletteConverter(JohnnyPaletteModel* model);
    ~JohnnyPaletteConverter();
    QImage convertImage(const QImage image);

private:
    void initPalette();

    int m_index_left_value;
    JohnnyPaletteModel* m_model;
    QVector<JohnnyPaletteItem* > m_palette;
    QVector<ColorReference* > m_color_references;
};

#endif // JOHNNYPALETTECONVERTER_H
