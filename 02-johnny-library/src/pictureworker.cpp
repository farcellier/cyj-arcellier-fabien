#include "pictureworker.h"

#include <QImage>
#include <qmath.h>
#include <climits>

PictureWorker::PictureWorker(QObject *parent) :
    QObject(parent),
    m_image(NULL)
{
}

PictureWorker::PictureWorker(const QImage *map, QObject *parent) :
    QObject(parent),
    m_image(map)
{
}

void PictureWorker::setImage(const QImage* image)
{
    this -> m_image = image;
}

/*!
 * Resize and Crop a picture to a specific size. The pictures will be
 * a square
 */
QImage PictureWorker::scaleAndCrop(int size) const
{
    int width = this -> m_image -> width();
    int height = this -> m_image -> height();
    QImage tmp_image_resize;

    if (width < height)
    {
        tmp_image_resize = this -> m_image -> scaledToWidth(size);
    }
    else
    {
        tmp_image_resize = this -> m_image -> scaledToHeight(size);
    }

    int width_resize = tmp_image_resize.width();
    int height_resize = tmp_image_resize.height();

    QImage tmp_image_cropped;
    if (height_resize > size)
    {
        int offset = (height_resize - size) / 2;
        tmp_image_cropped = tmp_image_resize.copy(0, offset, size, size);
    }
    else
    {
        int offset = (width_resize - size) / 2;
        tmp_image_cropped = tmp_image_resize.copy(offset, 0, size, size);
    }

    return tmp_image_cropped;
}

/*!
 * Extract the average color from this picture
 */
QColor PictureWorker::averageColor() const
{
    return this -> calculateColor(0, 0, this -> m_image -> width(), this -> m_image-> height());
}

/*!
 * Extract the average color from specific area
 */
QColor PictureWorker::averageColor(int x, int y, int size) const
{
    Q_ASSERT(x >= 0);
    Q_ASSERT(y >= 0);

    return this -> calculateColor(x, y, size, size);
}

QColor PictureWorker::calculateColor(int x, int y, int width, int height) const
{
    int distance_min = INT_MAX;
    QColor color;

    for(int i = x; i < x + width; i+=10)
    {
        for(int j = y; j < y + height; j+=10)
        {
            QRgb reference = this -> m_image -> pixel(i, j);
            int distance = 0;
            for(int k = x; k < x + width; k+=10)
            {
                for(int l = y; l < y + height; l+=10)
                {
                    QRgb pixel = this -> m_image -> pixel(k, l);
                    distance += this -> calculateDistance(reference, pixel);
                }
            }

            if (distance < distance_min)
            {
               color.setRgb(reference);
               distance_min = distance;
            }
        }
    }

    return color;
}

int PictureWorker::calculateDistance(QRgb reference, QRgb pixel) const
{
    return
                qPow(qBlue(reference) - qBlue(pixel), 2) +
                qPow(qRed(reference) - qRed(pixel), 2) +
                qPow(qGreen(reference) - qGreen(pixel), 2);
}
