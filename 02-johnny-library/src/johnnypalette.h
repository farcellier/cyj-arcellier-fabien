#ifndef JOHNNYPALETTE_H
#define JOHNNYPALETTE_H

#include <QColor>
#include <QImage>
#include <QList>

#include "ihandler.h"


class JohnnyPaletteItem
{
public:
    JohnnyPaletteItem(QImage image, QColor color);
    QImage image() const;
    QColor color() const;

private:
    QImage m_image;
    QColor m_averageColor;
};

class JohnnyPaletteModel : public QList<JohnnyPaletteItem>
{
public:
    JohnnyPaletteModel(IHandler* handler);
    void save();
    void load();
    static void load(QString path);

private:
    IHandler* m_handler;
};

class JohnnyPaletteHandler : public IHandler
{
public:
    JohnnyPaletteHandler(QString path);
    void save();
    void load();
    void setModel(JohnnyPaletteModel* model);

private:
    JohnnyPaletteModel* m_model;
    QString m_path;
};

#endif // JOHNNYPALETTE_H
