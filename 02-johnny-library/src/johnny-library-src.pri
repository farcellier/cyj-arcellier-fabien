SOURCES  *= $$PWD/pictureworker.cpp \
    $$PWD/johnnypalette.cpp \
    ../../02-johnny-library/src/johnnypaletteconverter.cpp

HEADERS  *= $$PWD/pictureworker.h \
    $$PWD/ihandler.h \
    $$PWD/johnnypalette.h \
    ../../02-johnny-library/src/johnnypaletteconverter.h

INCLUDEPATH *= $$PWD
