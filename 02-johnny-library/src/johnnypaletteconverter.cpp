#include "johnnypaletteconverter.h"
#include "johnnypalette.h"

#include "pictureworker.h"
#include <QPainter>
#include <QDebug>

const int PALETTE_SIZE = 65536;
const int COLOR_SIZE = 16;
const int PIXEL_SIZE = 20;

JohnnyPaletteConverter::JohnnyPaletteConverter(JohnnyPaletteModel *model) :
    m_model(model),
    m_palette(PALETTE_SIZE),
    m_color_references(m_model -> count())
{
    this -> m_index_left_value = 0;
    for (int i = 0; i < PALETTE_SIZE; i++)
    {
        this -> m_palette[i] = NULL;
    }

    this -> initPalette();
}

JohnnyPaletteConverter::~JohnnyPaletteConverter()
{

}

void JohnnyPaletteConverter::initPalette()
{
    // Calculer l'ensensemble des indexes
    for(int i = 0; i < this -> m_model -> count(); i++)
    {
        JohnnyPaletteItem* item = new JohnnyPaletteItem(this -> m_model -> at(i));
        QColor color = item -> color();
        this->m_color_references[i]= new ColorReference(color, item);
    }

    // Pour chaque pixel, calculer l'image la plus proche
    // Bleu
    for(int i = 0; i < COLOR_SIZE; i++)
    {
        // Rouge
        for(int j = 0; j < COLOR_SIZE; j++)
        {
            // Vert
            for(int k =0; k < COLOR_SIZE; k++)
            {
                QColor pixel_color(j, k, i);
                int distance_min = -1;
                JohnnyPaletteItem* item = NULL;
                for(int l = 0; l < this -> m_color_references.count(); l++)
                {
                    int distance = this -> m_color_references[i] -> distance(pixel_color);
                    JohnnyPaletteItem* item_tmp = this -> m_color_references[i] -> item();
                    item = distance_min == -1 || distance < distance_min ? item_tmp  : item;
                    distance_min = distance_min == -1 || distance < distance_min ? distance : distance_min;
                }

               int index = i * COLOR_SIZE * COLOR_SIZE + j * COLOR_SIZE + k;
               this -> m_palette[index] = item;
            }
        }
    }
}

/**
 * Convert an image to mosaic
 */
QImage JohnnyPaletteConverter::convertImage(const QImage image)
{
    QImage image_result(image);
    QPainter painter;
    bool paint_is_active = painter.begin(&image_result);

    if (paint_is_active == false)
        qDebug() << "Paint failed";

    PictureWorker worker(&image);

    for(int i = 0; i < image.width(); i += PIXEL_SIZE)
    {
        for(int j = 0; j < image.height(); j+= PIXEL_SIZE)
        {
            QColor average_color = worker.averageColor(i, j, PIXEL_SIZE);
            int index = (average_color.blue() / 16) * COLOR_SIZE * COLOR_SIZE + (average_color.red() / 16) * COLOR_SIZE + (average_color.green() / 16);

            QImage mosaic = this -> m_palette[index]->image();
            painter.drawImage(i, j, mosaic);
        }
    }

    painter.end();

    return image_result;
}
