#include "johnnypalette.h"
#include <QFile>

JohnnyPaletteItem::JohnnyPaletteItem(QImage image, QColor color):
    m_image(image),
    m_averageColor(color)
{
}

QImage JohnnyPaletteItem::image() const
{
    return this -> m_image;
}

QColor JohnnyPaletteItem::color() const
{
    return this -> m_averageColor;
}

JohnnyPaletteModel::JohnnyPaletteModel(IHandler *handler)
{
    this -> m_handler = handler;
}

void JohnnyPaletteModel::save()
{
    this -> m_handler -> save();
}

void JohnnyPaletteModel::load()
{
    this -> m_handler -> load();
}

void JohnnyPaletteModel::load(QString path)
{

}

JohnnyPaletteHandler::JohnnyPaletteHandler(QString path) :
    m_model(NULL)
{
    this -> m_path = path;
}

void JohnnyPaletteHandler::setModel(JohnnyPaletteModel* model)
{
    this -> m_model = model;
}

void JohnnyPaletteHandler::save()
{
    Q_CHECK_PTR(this -> m_model);

    QFile file(this -> m_path);
    file.open(QIODevice::WriteOnly);
    QDataStream stream(&file);
    for(int i = 0; i < this -> m_model -> count(); i++)
    {
        // Parcourir les elements du model
        // Stocker les infos dans un fichier binaire
        stream << this -> m_model -> at(i).color();
        stream << this -> m_model -> at(i).image();
    }

    file.close();
}

/*!
 *
 */
void JohnnyPaletteHandler::load()
{
    Q_CHECK_PTR(this -> m_model);
    this -> m_model -> clear();

    QFile file(this -> m_path);
    file.open(QIODevice::ReadOnly);
    QDataStream stream(&file);
    while(stream.atEnd() == false)
    {
        QColor color;
        stream >> color;
        QImage image;
        stream >> image;
        JohnnyPaletteItem item(image, color);
        this -> m_model -> append(item);
    }

    file.close();
}
