#ifndef PICTUREANALYZE_H
#define PICTUREANALYZE_H

#include <QObject>
#include <QColor>
class QImage;

class PictureWorker : public QObject
{
    Q_OBJECT
public:
    explicit PictureWorker(QObject *parent = 0);
    explicit PictureWorker(const QImage* map, QObject *parent = 0);
    QImage scaleAndCrop(int size) const;
    QColor averageColor() const;
    QColor averageColor(int x, int y, int size) const;
    void setImage(const QImage* image);


private:
    QColor calculateColor(int x, int y, int width, int height) const;
    int calculateDistance(QRgb reference, QRgb pixel) const;
    const QImage* m_image;
};

#endif // PICTUREANALYZE_H
