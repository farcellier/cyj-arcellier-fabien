#ifndef TST_PICTUREANALYSE_H
#define TST_PICTUREANALYSE_H

#include <QObject>

class tst_PictureWorker : public QObject
{
    Q_OBJECT
public:
    explicit tst_PictureWorker(QObject *parent = 0);
    
private slots:
    void testScaleAndCrop();
    void testAverageColorFull();
    
};

#endif // TST_PICTUREANALYSE_H
