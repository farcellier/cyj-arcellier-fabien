#include <QTest>
#include "tst_pictureworker.h"
#include "tst_johnnypalette.h"

/*!
 */
int main(int argc, char *argv[])
{
    tst_PictureWorker tst_picture_worker;
    tst_JohnnyPalette tst_johnny_palette;

    QTest::qExec(&tst_johnny_palette, argc, argv);
    QTest::qExec(&tst_picture_worker, argc, argv);

    return 0;
}
