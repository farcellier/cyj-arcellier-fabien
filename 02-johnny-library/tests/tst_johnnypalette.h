#ifndef TST_JOHNNYPALETTE_H
#define TST_JOHNNYPALETTE_H

#include <QObject>

class tst_JohnnyPalette : public QObject
{
    Q_OBJECT
public:
    explicit tst_JohnnyPalette(QObject *parent = 0);
    
private slots:
    void testSaveLoad();
    
};

#endif // TST_JOHNNYPALETTE_H
