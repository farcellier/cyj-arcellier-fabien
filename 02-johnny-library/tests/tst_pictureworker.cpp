#include "tst_pictureworker.h"

#include <QImage>
#include <QPoint>
#include <QTest>
#include "pictureworker.h"

tst_PictureWorker::tst_PictureWorker(QObject *parent) :
    QObject(parent)
{
}

void tst_PictureWorker::testScaleAndCrop()
{
    // Assign
    const int width = 170;
    const int height = 150;
    const QImage image(width, height, QImage::Format_RGB32);
    PictureWorker worker(&image);

    // Action
    QImage result = worker.scaleAndCrop(140);

    // Assert
    QCOMPARE(result.width(), 140);
    QCOMPARE(result.height(), 140);

}

void tst_PictureWorker::testAverageColorFull()
{
    // Assign
    const int width = 100;
    const int height = 100;
    QImage image(width, height, QImage::Format_RGB32);
    for(int i = 0; i < width; i++)
    {
        for(int j = 0; j < height; j++)
        {

            QColor color = i < width / 2 ? Qt::white : Qt::black;
            //QColor color = Qt::white;
            QRgb color_rgb = color.rgb();
            image.setPixel(i, j, color_rgb);
        }
    }

    PictureWorker analyze(&image, this);

    // Action
    QColor average_color = analyze.averageColor();
    int blue = average_color.blue();
    int green = average_color.green();
    int red = average_color.red();

    // Assert
    QCOMPARE(green, 127);
    QCOMPARE(red, 127);
    QCOMPARE(blue, 127);
}
