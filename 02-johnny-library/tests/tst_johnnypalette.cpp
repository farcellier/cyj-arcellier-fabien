#include "tst_johnnypalette.h"
#include "johnnypalette.h"

#include <QTest>

tst_JohnnyPalette::tst_JohnnyPalette(QObject *parent) :
    QObject(parent)
{
}

void tst_JohnnyPalette::testSaveLoad()
{
    JohnnyPaletteHandler handler("test.jpal");
    JohnnyPaletteModel model(&handler);
    handler.setModel(&model);

    JohnnyPaletteItem item(QImage(100, 100, QImage::Format_RGB32), Qt::white);
    model.append(item);

    JohnnyPaletteItem item1(QImage(100, 100, QImage::Format_RGB32), Qt::green);
    model.append(item1);

    model.save();
    model.clear();
    model.load();

    QCOMPARE(model.count(), 1);
}
