#-------------------------------------------------
#
# Project created by QtCreator 2013-03-22T14:13:33
#
#-------------------------------------------------

QT       += testlib core gui

TARGET = tst_mainwindow
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    tst_main.cpp \
    tst_pictureworker.cpp \
    tst_johnnypalette.cpp

HEADERS += tst_pictureworker.h \
    tst_johnnypalette.h

include(../src/johnny-library-src.pri)
